package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.persistance.Inventory;
import inventory.persistance.PartRepository;
import inventory.persistance.ProductRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class EntityIntegrationTest {

    private static final String fileName = "test/parts.txt";

    Inventory inventory;
    PartRepository partRepository;
    ProductRepository productRepository;
    InventoryService service;

    private Part part1;

    @BeforeEach
    void setUp() {
        clearFile();
        inventory  = new Inventory();
        productRepository = mock(ProductRepository.class);

        partRepository = new PartRepository(fileName, inventory);
        service = new InventoryService(partRepository, productRepository);

        part1 = new InhousePart(0,"AAA",5,5,1,10,2);
        inventory.addPart(part1);
    }

    @Test
    void addInhousePart() {
        service.addInhousePart("AAB",5,5,1,10,2);
        assertEquals(2, service.getAllParts().size());
    }

    @Test
    void getAllParts(){
        assertEquals(part1, service.getAllParts().get(0));
    }

    private void clearFile() {
        ClassLoader classLoader = ProductRepository.class.getClassLoader();
        try {
            File productsFile = new File(classLoader.getResource(fileName).getFile());
            new FileWriter(productsFile).close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
