package inventory.persistance;


import inventory.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.StringTokenizer;

public class ProductRepository {
	Logger logger = Logger.getLogger(ProductRepository.class);


	private String productsFilename = "data/products.txt";
	private Inventory inventory;

	public ProductRepository(Inventory inventory){
		this.inventory = inventory;
		readProducts();
	}

	//we need this because the unit tests will handle another file
	public ProductRepository(Inventory inventory, String fileName) {
		this.inventory = inventory;
		this.productsFilename = fileName;
		readProducts();
	}

	public void readProducts(){
		ClassLoader classLoader = ProductRepository.class.getClassLoader();
		File file = new File(classLoader.getResource(productsFilename).getFile());

		ObservableList<Product> listP = FXCollections.observableArrayList();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = null;
			while((line=br.readLine())!=null){
				Product product=getProductFromString(line);
				if (product!=null)
					listP.add(product);
			}
		} catch (IOException e) {
			logger.error(e.toString());
		}
		inventory.setProducts(listP);
	}

	private Product getProductFromString(String line){
		Product product=null;
		if (line==null|| line.equals("")) return null;
		StringTokenizer st=new StringTokenizer(line, ",");
		String type=st.nextToken();
		if (type.equals("P")) {
			int id= Integer.parseInt(st.nextToken());
			inventory.setAutoProductId(id);
			String name= st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			String partIDs=st.nextToken();

			StringTokenizer ids= new StringTokenizer(partIDs,":");
			ObservableList<Part> list= FXCollections.observableArrayList();
			while (ids.hasMoreTokens()) {
				String idP = ids.nextToken();
				Part part = inventory.lookupPart(idP);
				if (part != null)
					list.add(part);
			}
			product = new Product(id, name, price, inStock, minStock, maxStock, list);
			product.setAssociatedParts(list);
		}
		return product;
	}

	public void writeAllProducts() {
		ClassLoader classLoader = ProductRepository.class.getClassLoader();
		File productsFile = new File(classLoader.getResource(productsFilename).getFile());

		ObservableList<Product> products=inventory.getProducts();

		try (BufferedWriter bw = new BufferedWriter(new FileWriter(productsFile))) {
			for (Product pr:products) {
				StringBuilder line= new StringBuilder(pr.toString() + ",");
				ObservableList<Part> list= pr.getAssociatedParts();
				int index=0;
				while(index<list.size()-1){
					line.append(list.get(index).getPartId()).append(":");
					index++;
				}
				if (index==list.size()-1)
					line.append(list.get(index).getPartId());
				bw.write(line.toString());
				bw.newLine();
			}
		} catch (IOException e) {
			logger.error(e.toString());
		}
	}

	public void addProduct(Product product){
		inventory.addProduct(product);
		writeAllProducts();
	}

	public int getAutoProductId(){
		return inventory.getAutoProductId();
	}

	public ObservableList<Product> getAllProducts(){
		return inventory.getProducts();
	}

	public Product lookupProduct (String search){
		return inventory.lookupProduct(search);
	}

	public void updateProduct(int productIndex, Product product){
		inventory.updateProduct(productIndex, product);
		writeAllProducts();
	}

	public void deleteProduct(Product product){
		inventory.removeProduct(product);
		writeAllProducts();
	}
}
